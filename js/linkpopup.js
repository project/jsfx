
/**
 * Make certain links open in a new window.
 */
$(document).ready(function() {
var signal = Drupal.settings.linkpopup.signal;
  $("//a[@title='" + signal + "']").each(function(){
    $(this).attr('target', '_blank');
  });
  
  $(".block").each(function(){
    if (Drupal.settings.linkpopup.blocks[this.id]) {
      $(this).find('a').each(function(){
      
        if($(this).attr('href').match('http://')) {
          $(this).attr('target', '_blank');
        }

      });
    }
  });
});