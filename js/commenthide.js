
/**
 * Hide comment bodies.
 */
$(document).ready(function () {

var expanded = false;
var commentsHeight = $('#comments').height();      
var imageHeight;     

    
$("#comments/.comment").each(function(i) {  

$(this).find('.content').hide();
$(this).find('.links').hide();

});


$("#expand_comments").click(function(){
    
     $('#comments').height(commentsHeight);   
      
       
     $("#comments/.comment").each(function(i) {
     var picture = $(this).find('.picture');
     var content = $(this).find('.content');
     var links = $(this).find('.links');    
      
      content.toggle();
      links.toggle();
      
     }); 
     
     expanded = !expanded;
     $("#expand_comments").html(expanded ? 'Hide Comments' : 'Expand Comments');
  
}); 
});