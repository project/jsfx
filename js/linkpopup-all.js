
/**
 * Make all absolute links open in a new window.
 */
$(document).ready(function() {
var signal = Drupal.settings.linkpopup;
  $("a").each(function(){
  
    if($(this).attr('href').match('http://')) {
      $(this).attr('target', '_blank');
    }
    
  });

});