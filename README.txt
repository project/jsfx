
Introduction

  Created by silviogutierrez, http://the.failbo.at

  JSFX is a suite of effects for Drupal. Everything is contained in one module, but the effects can be turned on and off separately. 
  Currently, the suite includes:
    - Memoryblock: Similar to Collapsiblock in JS Tools, but with completely different code. 
      Activating this effect will make all your blocks collapsible. Using cookies, it will remember the state each block was left in.
    - Commenthide: This effect will hide the body of all your comments, and leave the title, submission info, and the avatar, if any.
      The effect will add an "Expand Comments" link which will show the bodies of all your comments when clicked.
    - Link PopUp: If you use XHML Strict, you know that the attribute "target" is forbidden, so making links open in a new window can be a hassle.
      This effect will make certain links open in a new window. By default, it applies to absolute links in the entire site. See notes.

Installation:
    1. Copy the contents of the module into a JSFX folder in your modules directory.
    2. Enable the module.
    2. Go to JSFX Settings in Site Configuration. Path: /admin/settings/JSFX
    3. Enable desired effects, and configure.
    
Notes:
  Memoryblock:
    - The main difference with Collapsiblock and Memoryblock is that Memoryblock is designed to be used with blocks that use images.
      Memoryblock lets you use a different image for the top of closed blocks, and open blocks. 
      Closed blocks tops will have the class blockTop and open ones will have blockTopOpen, theme appropriately.
    - Details of Block Opening: 
        1. Block Top class changes to blockTopOpen.
        2. Bottom slides down.
    - Details of Block Closing:
        1. Bottom slides up.
        2. Block Top class changes to blockTop.
    - First element inside each .block must be the top. Second element must be the bottom.
  
  Link PopUp:
    - If you use Selective Mode, JSFX will look for links marked with a title of External.
      Example: If you use TinyMCE, when inserting external links, add an "External" title.
      This will signal Link PopUp, and also informs your reader that the link pops when the hovering.
    - If you want to use another signal other than "External", replace it with a custom one.
      Remember the signal is case sensitive, so aim for a simple signal, such as "New Window".
    - In Selective Mode, you may go into each block's configuration and make all absolute links open in new windows.
      This setting is on a per block basis.
    - Absolute Link: links containing "http://".
    
  

Troubleshooting:

  General:
    - Please send all issues to silviogutierrez@yahoo.com, or visit http://the.failbo.at to discuss this release.

  Memoryblock:
    - The UL in block bottoms must NOT have a margin on top, otherwise the content will shift slightly after each expansion. Use padding instead.
    
  Commenthide:
    - Firefox flashes whenever you are at the bottom of the page and the height of an element changes. 
      To avoid this, the original height of the page is NOT restored when you close the comments. This is not a bug.
      
Credits
  - Thanks to quiptime and jptavan for the Memoryblock bug fix which messed up the classes.